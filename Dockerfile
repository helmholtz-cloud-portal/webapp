FROM adoptopenjdk:11-jre-hotspot
ENV CLIENT_SECRET nopass
COPY target/hm-server-package.tar hms.tar
EXPOSE 8080
CMD tar -xvf hms.tar -C ./ && cd ./hm-server/bin && ./start.sh -s $CLIENT_SECRET -a dev
