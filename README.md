Helmholtz Marketplace Web App
==============================
![](https://github.com/helmholtz-marketplace/helmholtz-marketplace-webapp/workflows/CI/badge.svg)

An entry point web application for scientific related services from various vendors.

### Automatic deployments of Merge Requests

Each Merge Request will be automatically deployed to a Kubernetes test cluster so that you can directly see your changes in a live deployment. It will be set up together with the Cerebrum backend with its own database. For Cerebrum the code from its current master branch will be taken. The deployment is running in a pipeline at DESY. You can find a link to that pipeline in the output of the last job in the CI/CD pipeline (`trigger_deploy_mr`). There you can also find a url where you access the Cloud Portal when it has been deployed. When you push new commits to the MR the application will be redeployed.

### Running the CI in your fork

If you want to run the SonarQube tests in your fork, you will have to create a GitHub secret with the name `SONAR_AUTH_TOKEN` in your repository. You can get the necessary token in SonarQube via "My Account" -> "Security" -> "Tokens".

### List a new Helmholtz Cloud Service

Your service is connected to the Helmholtz AAI und ready to be listed in our Cloud Portal. This is how to do that:

#### 1. Request for cloud service listing
1. Create a new **Issue** [Click here: New Issue "Cloud Listing Request"](https://gitlab.hzdr.de/hifis-technical-platform/helmholtz-marketplace-webapp/-/issues/new?issuable_template=cloud_listing_request)
2. Name your issue (e.g. "Service Description: YOUR_SERVICE_NAME")
3. In field **description** select the template **cloud_listing_request**. The textbox below will be filled with a template
4. Modify the content of the textbox, enter the information as requested
5. Select an **Assignee**, e.g.:
    - @jandt-desy 
    - @andreasfk 
6. Click on **Create issue**

#### 2. Inspect Issue
Your issue will then be reviewed.
