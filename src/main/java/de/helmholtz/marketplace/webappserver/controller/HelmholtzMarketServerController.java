package de.helmholtz.marketplace.webappserver.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class HelmholtzMarketServerController
{
    @Value("${cerebrum.endpoint}")
    String cerebrumUrl;

    private final OAuth2AuthorizedClientService authorizedClientService;

    public HelmholtzMarketServerController(
            OAuth2AuthorizedClientService authorizedClientService)
    {
        this.authorizedClientService = authorizedClientService;
    }

    @GetMapping(path = "/tokens", produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonNode getAccessToken(OAuth2AuthenticationToken authentication,
                              HttpServletResponse response) throws IOException
    {
        if (authentication == null) {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "OAuth2AuthenticationToken is null");
            return null;
        }
        final ObjectMapper mapper = new ObjectMapper();
        OAuth2AuthorizedClient client = authorizedClientService.loadAuthorizedClient(
                authentication.getAuthorizedClientRegistrationId(), authentication.getName());
        return mapper.readTree("{ \"access_token\" : \"" + client.getAccessToken().getTokenValue() + "\"}");
    }

    @GetMapping("/csrf")
    public CsrfToken csrf(CsrfToken token)
    {
        return token;
    }

    @GetMapping(path = "/scripts/config.js", produces = "text/javascript")
    public String getWebAppConfig()
    {
        return "var CONFIG = {\"cerebrum.endpoint\": \"" + cerebrumUrl +"\"}";
    }
}
